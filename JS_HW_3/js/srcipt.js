let firstValue="";
let secondValue="";
let operator="";
let isCancelled = false;

//====================data entering + Cancel operation ==============================
do {
    firstValue = prompt("Please enter first value",firstValue);
    if (!firstValue) {isCancelled = true; break;}
    secondValue = prompt("Please enter second value",secondValue);
    if (!secondValue) {isCancelled = true; break;}
    operator= prompt("Enter `+`, `-`, `*`, `/`");
    if (!operator) {isCancelled = true; break;}
}
while (!IsValid(firstValue, secondValue, operator));

//====================checking correctness(Data validation) of entered data===========
function IsValid(firstValue, secondValue, operator)
{
    let result = true;
    result = result && !isNaN(firstValue);
    result = result && !isNaN(secondValue);
    result = result && (operator == "+" || operator == "-" || operator == "/" || operator == "*");

    return result;
}

//=================Calculation function ==============
function calculation(firstValue, secondValue, operator) {
    if (operator=="+"){
        return firstValue+secondValue;
    }
    if (operator=="-"){
        return firstValue-secondValue;
    }
    if (operator=="/"){
        return firstValue/secondValue;
    }
    if (operator=="*"){
        return firstValue*secondValue;
    }
}

// ======================Result printing ===========================
if (!isCancelled)
{
    let sum=calculation(+firstValue, +secondValue, operator);
    console.log(sum);
  //alert(sum);// may print result to browser modal windows

}